export default {
  DECREASE_TIME_LEFT: 'DECREASE_TIME_LEFT',
  INCREASE_POINTS: 'INCREASE_POINTS',
  INCREASE_TIME_LEFT: 'INCREASE_TIME_LEFT',
  IS_PAUSED: 'IS_PAUSED'
}
