import types from './mutation-types'

export default {
  decreaseTimeLeft ({ commit }, seconds) {
    commit(types.DECREASE_TIME_LEFT, seconds)
  },
  increasePoints ({ commit }, points) {
    commit(types.INCREASE_POINTS, points)
  },
  increaseTimeLeft ({ commit }, seconds) {
    commit(types.INCREASE_TIME_LEFT, seconds)
  },
  setPaused ({ commit }, isPaused) {
    commit(types.IS_PAUSED, isPaused)
  }
}
