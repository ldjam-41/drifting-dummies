import types from './mutation-types'

export default {
  [types.DECREASE_TIME_LEFT] (state, seconds) {
    state.timeLeftInSeconds -= seconds
  },
  [types.INCREASE_POINTS] (state, points) {
    state.points += points
  },
  [types.INCREASE_TIME_LEFT] (state, seconds) {
    state.timeLeftInSeconds += seconds
  },
  [types.IS_PAUSED] (state, isPaused) {
    state.isPaused = isPaused
  }
}
