export default () => ({
  availableDummies: [
    'gelb',
    'gruen',
    'rot'
  ].map(color => ({
    animationUrl: require(`~/assets/helion_${color}.gif`),
    imageUrl: require(`~/assets/helion_${color}.png`)
  })),
  isPaused: true,
  points: 0,
  timeLeftInSeconds: 30
})
