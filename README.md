# [Drifting Dummies](https://ldjam-41.gitlab.io/drifting-dummies/)

code is licensed under MIT, graphics and music under CC BY-NC-SA 4.0

## Local Setup

``` bash
# install dependencies
$ npm install # Or yarn install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm start

# generate static project
$ npm run generate
```

For detailed explanation on how things work, checkout the [Nuxt.js docs](https://github.com/nuxt/nuxt.js).
